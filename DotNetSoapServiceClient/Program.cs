﻿using System;
using System.Diagnostics;
using System.Net;
using System.ServiceModel;
using System.Threading.Tasks;
using AccountServiceReference;

namespace DotNetSoapServiceClient
{
    class Program
    {
        static async Task Main(string[] args)
        {

            var proxy = Environment.GetEnvironmentVariable("HTTP_PROXY");
            if (!string.IsNullOrWhiteSpace(proxy))
            {
                Console.WriteLine($"Proxy: {proxy}");
                WebRequest.DefaultWebProxy = new WebProxy(proxy, false);
            }

            var targetUrl = Environment.GetEnvironmentVariable("TARGET_URL");
            if (string.IsNullOrWhiteSpace(targetUrl))
            {
                Console.WriteLine("Error, please set TARGET_URL environment variable.");
                return;
            }

            if (!Uri.TryCreate(targetUrl, UriKind.RelativeOrAbsolute, out var targetUri))
            {
                Console.WriteLine("Error, please set TARGET_URL should be a valid Uri.");
                return;
            }

            var binding =
                string.Compare(targetUri.Scheme, Uri.UriSchemeHttp, StringComparison.InvariantCultureIgnoreCase) == 0
                    ? new BasicHttpBinding()
                    : new BasicHttpsBinding() as System.ServiceModel.Channels.Binding;
            var service = new AccountServiceClient(
                binding: binding,
                remoteAddress: new EndpointAddress(
                    uri: new Uri($"{targetUrl}/AccountService.asmx")
                )
            );

            Console.WriteLine($"Calling AccountServer at '{targetUrl}/AccountService.asmx'");

            try
            {
                await service.DeleteAllAccountsAsync();
                var getAllAccountsResponse = await service.GetAllAccountsAsync();

                var account = new Account
                {
                    UserId = -1,
                    User = "dd",
                    First = "John",
                    Last = "Smith",
                    Password = "Passowrd!"
                };
                account = await service.CreateAccountAsync(account);
                Debug.Assert(account != null, "`account` is null");
                Debug.Assert(account.UserId > 0, "UserId is not greater or equals than 0.");

                account.First = "Jain";
                await service.UpdateAccountAsync(account);
                account = await service.GetAccountByIdAsync(account.UserId);

                Debug.Assert(account != null, "`account` is null");
                Debug.Assert(account.First == "Jain", "Account.First contains outdated value");

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Debug.Flush();
            }
        }
    }
}
