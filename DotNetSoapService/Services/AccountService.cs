﻿using System;

namespace DotNetSoapService.Services
{
    public class AccountService : IAccountService
    {
        public Account CreateAccount(Account account)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));

            return Repository.Create(account);
        }

        public Account GetAccountById(int userId)
            => Repository.GetAccount(userId);

        public Account GetAccountByUser(string user)
            => Repository.GetAccount(user);

        public void UpdateAccount(Account account)
        {
            if (account == null) throw new ArgumentNullException(nameof(account));

            Repository.Save(account);
        }

        public Account[] GetAllAccounts()
            => Repository.GetAccounts();

        public void DeleteAccount(int userid)
            => Repository.Delete(userid);

        public void DeleteAllAccounts()
            => Repository.DeleteAll();
    }
}
