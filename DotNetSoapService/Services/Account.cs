﻿using System.Runtime.Serialization;

namespace DotNetSoapService.Services
{
    [DataContract]
    public class Account
    {
        [DataMember]
        public int UserId { get; set; }

        [DataMember]
        public string User { get; set; }

        [DataMember]
        public string First { get; set; }

        [DataMember]
        public string Last { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}