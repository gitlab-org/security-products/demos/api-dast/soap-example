using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.ServiceModel;
using DotNetSoapService.Services;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SoapCore;

namespace DotNetSoapService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.TryAddSingleton<IAccountService, AccountService>();

            services.AddSoapCore();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                var binding = new BasicHttpBinding();
                endpoints.UseSoapEndpoint<IAccountService>(
                    path: $"/{nameof(AccountService)}.asmx",
                    binding: binding,
                    serializer: SoapSerializer.DataContractSerializer,
                    caseInsensitivePath: true
                );
            });
        }
    }
}
